CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Using Views
 * Maintainers

INTRODUCTION
------------

The Zurb Views module adds styles to Views to output the results of a view 
as several common Zurb foundation components.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/views_zurb

REQUIREMENTS
------------

This module requires the following themes/modules:

 * Views (Core)
 * Zurb Foundation Theme (https://www.drupal.org/project/zurb_foundation)

INSTALLATION
------------

 * Install the Zurb Views module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/node-id for
   further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.

USING VIEWS
-----------

    1. Add style type and select which zurb foundation component style you wish to use
    2. Change the settings for that specific component


MAINTAINERS
-----------

 * Vinit Kumar (aburrows) - https://www.drupal.org/u/vinitk
