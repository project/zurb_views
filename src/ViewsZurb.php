<?php

namespace Drupal\views_zurb;

use Drupal\Component\Utility\Html;
use Drupal\views\ViewExecutable;

/**
 * The primary class for the Views Bootstrap module.
 *
 * Provides many helper methods.
 *
 * @ingroup utility
 */
class ViewsZurb {

  /**
   * Returns the theme hook definition information.
   */
  public static function getThemeHooks() {
    $hooks['views_zurb_accordion'] = [
      'preprocess functions' => [
        'template_preprocess_views_zurb_accordion',
        'template_preprocess_views_view_accordion',
      ],
      'file' => 'views_zurb.theme.inc',
    ];
    $hooks['views_zurb_block_grid'] = [
      'preprocess functions' => [
        'template_preprocess_views_zurb_block_grid',
        'template_preprocess_views_view',
      ],
      'file' => 'views_zurb_foundation.theme.inc',
    ];


    return $hooks;
  }


  /**
   * Return an array of breakpoint names.
   */
  public static function getBreakpoints() {
    return ['small', 'medium', 'large'];
  }

  /**
   * Get column class prefix for the breakpoint.
   */
  public static function getColumnPrefix($breakpoint) {
    return ($breakpoint != 'small' ? '-' . $breakpoint : '');
  }

  /**
   * Get unique element id.
   *
   * @param \Drupal\views\ViewExecutable $view
   *   A ViewExecutable object.
   *
   * @return string
   *   A unique id for an HTML element.
   */
  public static function getUniqueId(ViewExecutable $view) {
    $id = $view->storage->id() . '-' . $view->current_display;
    return Html::getUniqueId('views-zurb-' . $id);
  }

  /**
   * Get the number of items from the column class string.
   *
   * @param string $size
   *   Bootstrap grid size xs|sm|md|lg.
   *
   * @return int|false
   *   Number of columns in a 12 column grid or false.
   */
  public static function getColSize($size) {
    if (preg_match('~col-[a-z]{2}-([0-9]*)~', $size, $matches)) {
      return 12 / $matches[1];
    }

    return FALSE;
  }

}
