<?php

/**
 * @file
 * Preprocessors and helper functions to make theming easier.
 */

use Drupal\views_zurb\ViewsZurb;
use Drupal\Core\Template\Attribute;

/**
 * Prepares variables for views accordion templates.
 *
 * Default template: views-zurb-accordion.html.twig.
 *
 * @param array $vars
 *   An associative array containing:
 *   - view: A ViewExecutable object.
 *   - rows: The raw row data.
 */
function template_preprocess_views_zurb_accordion(array &$vars) {
  $view = $vars['view'];
  $vars['id'] = ViewsZurb::getUniqueId($view);
  $panel_title_field = $view->style_plugin->options['panel_title_field'];
  $vars['attributes']['class'][] = 'panel-group';
  $vars['options'] = $view->style_plugin->options;
  if ($panel_title_field) {
    foreach ($vars['rows'] as $id => $row) {
      $vars['rows'][$id] = [];
      $vars['rows'][$id]['content'] = $row;
      $vars['rows'][$id]['title'] = $view->style_plugin->getField($id, $panel_title_field);
    }
  }
  else {
    // @TODO: This would be better as valdiation errors on the style plugin options form.
    \Drupal::messenger()->addWarning(t('@style style will not display without the "@field" setting.',
      [
        '@style' => $view->style_plugin->definition['title'],
        '@field' => 'Panel title',
      ]
    ));
  }
  // @TODO: Make sure that $vars['rows'] is rendered array.
  // @SEE: Have a look template_preprocess_views_view_unformatted()
  // and views-view-unformatted.html.twig
}

function template_preprocess_views_zurb_block_grid(array &$vars) {
  $view = $vars['view'];
  $vars['id'] = ViewsZurb::getUniqueId($view);
  $vars['attributes']['class'][] = 'grid';
  $options = $view->style_plugin->options;

  $options['block_grid_breakpoints'] = [
    'small' => $options['small'],
    'medium' => $options['medium'],
    'large' => $options['large'],
  ];

  $options['block_grid'] = implode(' ', $options['block_grid_breakpoints']);

  $vars['options'] = $options;
}

